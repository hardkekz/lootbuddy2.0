﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using AOSharp.Core;
//using Newtonsoft.Json;
//using AOSharp.Core.UI;

//namespace LootBuddy
//{
//    public class Config
//    {
//        public Dictionary<int, CharacterSettings> CharSettings { get; set; }

//        protected string _path;

//        public static string Location = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootBuddy\\Config.json";

//        [JsonIgnore]
//        public int IPCChannel => CharSettings != null && CharSettings.ContainsKey(Game.ClientInst) ? CharSettings[Game.ClientInst].IPCChannel : 0;

//        public static Config Load(string path)
//        {
//            Config config;
//            try
//            {
//                config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(path));

//                config._path = path;
//            }
//            catch
//            {
//                Chat.WriteLine($"No config file found.");
//                Chat.WriteLine($"Using default settings");

//                config = new Config
//                {
//                    CharSettings = new Dictionary<int, CharacterSettings>()
//                    {
//                        { Game.ClientInst, new CharacterSettings() }
//                    }
//                };

//                config._path = path;

//                config.Save();
//            }

//            return config;
//        }

//        public void Save()
//        {
//            if(!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
//                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

//            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootBuddy"))
//                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootBuddy");

//            File.WriteAllText(Location, JsonConvert.SerializeObject(this, Formatting.Indented));
//        }
//    }

//    public class CharacterSettings
//    {
//        public int IPCChannel { get; set; } = 0;
//    }
//}
