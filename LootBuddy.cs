﻿using AOSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Common.GameData;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using AOSharp.Core.IPC;
using AOSharp.Common.GameData.UI;
using System.Threading.Tasks;

namespace LootBuddy
{
    public class Main : AOPluginEntry
    {
        private double _lastCheckTime = Time.NormalTime;
        private AOSharp.Core.Settings LootBuddySettings = new AOSharp.Core.Settings("LootBuddy");

        public static Container extrabag1 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra1");
        public static Container extrabag2 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra2");
        public static Container extrabag3 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra3");
        public static Container extrabag4 = Inventory.Backpacks.FirstOrDefault(x => x.Name == "extra4");

        public static Corpse corpsesToLoot;

        public static List<string> IgnoreItems = new List<string>();

        public string LootingRulesLocation = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootBuddy\\LootingRules.json";
        //public string ConfigLocation = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootBuddy\\{Game.ClientInst}\\Config.json";

        public static bool Start = true;

        public static string PluginDirectory;

        //public static IPCChannel IPCChannel { get; private set; }

        //public static Config Config { get; private set; }

        public override void Run(string pluginDir)
        {
            try
            {
                PluginDirectory = pluginDir;

                //Config = Config.Load(ConfigLocation);
                LootingRules.Load(LootingRulesLocation);

                //IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                Chat.WriteLine("LootBuddy loaded!");
                Chat.WriteLine("/lootbuddy for settings.");

                LootBuddySettings.AddVariable("Running", false);
                LootBuddySettings.AddVariable("ApplyRules", false);
                LootBuddySettings.AddVariable("SingleItem", false);
                LootBuddySettings.AddVariable("PredefinedFilters", false);
                LootBuddySettings.AddVariable("IncludeSpec", false);
                LootBuddySettings.AddVariable("IncludeClumps", false);

                LootBuddySettings["Running"] = false;

                //Chat.RegisterCommand("buddy", LootBuddyCommand);

                SettingsController.RegisterSettingsWindow("LootBuddy", pluginDir + "\\UI\\LootBuddySettingWindow.xml", LootBuddySettings);

                Game.OnUpdate += OnUpdate;
                Inventory.ContainerOpened = OnContainerOpened;
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private bool ItemExists(Item item)
        {
            if (extrabag1 != null && !extrabag1.Items.Contains(item) || (extrabag2 != null && !extrabag2.Items.Contains(item))
                 || (extrabag3 != null && !extrabag3.Items.Contains(item)) || (extrabag4 != null && !extrabag4.Items.Contains(item))
                 || !Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Contains(item))
                return false;
            else
                return true;
        }

        private void OnContainerOpened(object sender, Container container)
        {
            if (!LootBuddySettings["Running"].AsBool()) { return; }

            if (container.Identity.Type == IdentityType.Corpse && container.Items.Count >= 0)
            {
                foreach (Item item in container.Items)
                {
                    if (LootBuddySettings["ApplyRules"].AsBool())
                    {
                        if (LootingRules.Apply(item))
                        {
                            if (LootBuddySettings["SingleItem"].AsBool() && !ItemExists(item))
                                continue;

                            if (Inventory.NumFreeSlots >= 1)
                                item.MoveToInventory();
                            else
                            {
                                MoveItem(item);
                            }
                        }
                        else
                        {
                            item.Delete();
                        }
                    }
                    else
                    {
                        if (
                        LootBuddySettings["PredefinedFilters"].AsBool() && (
                        !(
                        item.Name.Contains("Teleportation")
                        || item.Name.Contains("Activation")
                        || item.Name.Contains("Insignia")
                        || item.Name.Contains("Alien Armor")
                        || (item.Name.Contains("Pearl") && !item.Name.Contains("Ruby"))
                        || item.Name.Contains("Sancrosanct Rifle")
                        //|| item.Name.Contains("Clump")
                        || item.Name.Contains("Fuel")
                        || item.Name.Contains("Broken")
                        || item.Name.Contains("Refined")
                        || item.Name.Contains("Predator")
                        || item.Name.Contains("Defense")
                        || item.Name.Contains("Combat")
                       || (item.Name.Contains("Nano") && !item.Name.Contains("Input Hood"))
                        //|| item.Name.Contains("Canister of Pure Liquid Notum")
                        || item.Name.Contains("Illegal")
                        || item.Name.Contains("Pattern")
                        || item.Name.Contains("Source")
                        || item.Name.Contains("Muzzle")
                        || item.Name.Contains("Cataclysmic")
                        || item.Name.Contains("Memories")
                        || item.Name.Contains("Ring")
                        || (item.Name.Contains("Miy") && item.QualityLevel.Equals(300))
                        || (item.Name.Contains("Miy") && item.QualityLevel.Equals(220))
                        //|| (item.Name.Contains("Kyr") && item.QualityLevel.Equals(300))
                        //|| (item.Name.Contains("Kyr") && item.QualityLevel.Equals(250))
                        //|| (item.Name.Contains("Kyr") && item.QualityLevel.Equals(200))
                        || item.Name.Contains("Symbiant")
                        || item.Name.Contains("Dormant")
                        || item.Name.Contains("Evening")

                        || (LootBuddySettings["IncludeClumps"].AsBool() &&
                            // spec 1-4
                            (item.Name.Contains("Clump")
                            )
                        )

                        || (LootBuddySettings["IncludeSpec"].AsBool() &&
                            // spec 1-4
                            (item.Name.Contains("Mariner")
                            || item.Name.Contains("Afirce")
                            || item.Name.Contains("Girl")
                            || item.Name.Contains("Kanel")
                            )
                        )

                        || item.Name.Contains("Light Perennium Container")

                        || item.Name.Contains("Ancient")
                        || item.Name.Contains("Inert Knowledge")
                        || item.Name.Contains("Empty Ancient Device")
                        || item.Name.Contains("Bracer")

                        // all the spirits needed to build alphas
                        || item.Name.Contains("Dispirited Spirit of Essence")
                        || item.Name.Contains("Sorry Spirit of Discerning Weakness")
                        || item.Name.Contains("Poor Essence Brain Spirit")
                        || item.Name.Contains("Sorrowful Spirit of Clear Thought")
                        || item.Name.Contains("Sorry Brain Spirit of Offence")
                        || item.Name.Contains("Wistful Brain Spirit of Computer Skill")
                        || item.Name.Contains("Somber Spirit of Knowledge Whispered")
                        || item.Name.Contains("Sorrowful Spirit of Strength Whispered")
                        || item.Name.Contains("Tragic Spirit of Essence Whispered")
                        || item.Name.Contains("Rejected Right Limb Spirit of Weakness")
                        || item.Name.Contains("Troubled Right Limb Spirit of Essence")
                        || item.Name.Contains("Tragic Right Limb Spirit of Strength")
                        || item.Name.Contains("Sorry Heart Spirit of Knowledge")
                        || item.Name.Contains("Sorry Heart Spirit of Strength")
                        || item.Name.Contains("Tragic Heart Spirit of Essence")
                        || item.Name.Contains("Tragic Heart Spirit of Weakness")
                        || item.Name.Contains("Sorry Left Limb Spirit of Weakness")
                        || item.Name.Contains("Tragic Left Limb Spirit of Essence")
                        || item.Name.Contains("Tragic Left Limb Spirit of Understanding")
                        || item.Name.Contains("Tragic Left Limb Spirit of Strength")
                        || item.Name.Contains("Dispirited Spirit of Right Wrist Offence")
                        || item.Name.Contains("Dispirited Spirit of Right Wrist Weakness")
                        || item.Name.Contains("Sorrowful Midriff Spirit of Weakness")
                        || item.Name.Contains("Tragic Midriff Spirit of Essence")
                        || item.Name.Contains("Tragic Midriff Spirit of Strength")
                        || item.Name.Contains("Troubled Midriff Spirit of Knowledge")
                        || item.Name.Contains("Sorrowful Spirit of Left Wrist Strength")
                        || item.Name.Contains("Tragic Spirit of Left Wrist Defense")
                        || item.Name.Contains("Sorrowful Right Hand Strength Spirit")
                        || item.Name.Contains("Tragic Right Hand Defencive Spirit")
                        || item.Name.Contains("Troubled Spirit of Insight -Right Hand")
                        || item.Name.Contains("Sorrowful Spirit of Defense")
                        || item.Name.Contains("Troubled Spirit of Essence")
                        || item.Name.Contains("Somber Left Hand Spirit of Strength")
                        || item.Name.Contains("Tragic Left Hand Spirit of Defence")
                        || item.Name.Contains("Tragic Spirit of Feet Strength")
                        || item.Name.Contains("Troubled Spirit of Feet Defense")


                        || item.Name.Contains("Spirit Of Strength")
                        || item.Name.Contains("Spirit Of Right Wrist Offense")

                        || item.Name.Contains("Offense")
                        || item.Name.Contains("Blue Glyph")
                        || item.Name.Contains("Furious Novictum")
                        || item.Name.Contains("Xan Essence")
                        || item.Name.Contains("Wistful")
                        || item.Name.Contains("Miy")
                        || item.Name.Contains("Mantidae")
                        || item.Name.Contains("Instruction Disc")
                        || item.Name.Contains("Egg")
                        //                            || item.Name.Contains("Crystal")
                        || item.Name.Contains("Ultra-Light Missile Pistol Raid 9-X")
                        || item.Name.Contains("Hot Stone")
                        || item.Name.Contains("Queen Blade")
                        || item.Name.Contains("Soul Stone")
                        //                           || item.Name.Contains("Robot Junk")
                        //                           || item.Name.Contains("Gold")
                        || (item.Name.Contains("Fragment") && !item.Name.Contains("Crystal"))
                        || (item.Name.Contains("Novictum") && !item.Name.Contains("Spangled"))
                        || item.Name.Contains("Ljotur")
                        || item.Name.Contains("Leet")
                        || item.Name.Contains("Nano Crystal")
                        || item.Name.Contains("Pest")
                        || item.Name.Contains("Chain of Concealment")
                        || item.Name.Contains("The HSR Detective")
                        || item.Name.Contains("Dog")
                        || item.Name.Contains("Home Defender")
                        || item.Name.Contains("Soul Capsule")
                        || item.Name.Contains("Nippy")

                        || item.Name.Contains("Yomi")
                        || item.Name.Contains("Vanya's Beating Heart")


                        //|| item.Name.Contains("Perennium")
                        || item.Name.Contains("Mortiig")
                        || item.Name.Contains("Apparatus")
                        //|| item.Name.Contains("Monster Parts")
                        //|| item.Name.Contains("Armor")

                        || item.Name.Contains("Gem of the")
                        || item.Name.Contains("Spirit Training")
                        || item.Name.Contains("Problem Device")
                        || item.Name.Contains("Cure for Baldness")
                        || (item.Name.Contains("Visions") && !item.Name.Contains("Empty"))
                        || item.Name.Contains("Embryo")

                        || item.Name.Contains("Cleaver")
                        || item.Name.Contains("Illuminated")
                        || item.Name.Contains("Cracker Jack")
                        || item.Name.Contains("Embryo")
                        || item.Name.Contains("Personal Pistol")
                        || item.Name.Contains("Jame Blaster")
                        || item.Name.Contains("Nophex Cube")
                        || (item.Name.Contains("Basic") && item.Name.Contains("Implant") && item.QualityLevel.Equals(200))
                        || (item.Name.Contains("Cluster") && item.Name.Contains("Regeneration"))
                        || (item.Name.Contains("Cluster") && item.Name.Contains("Delta"))
                        || item.Name.Contains("Controller Recompiler")
                        || item.Name.Contains("Cell Templates")
                        || item.Name.Contains("Plasmid")
                        || item.Name.Contains("Protein Mapping Data")
                        || item.Name.Contains("Mitochondria")



                            )))
                        {
                            item.Delete();
                        }
                        else
                        {
                            MoveItem(item);
                        }
                    }
                }
            }
        }

        private void MoveItem(Item item)
        {
            //todo: doesn't belong here
            foreach (Item item2 in Inventory.Items.Where(c => c.Slot.Type == IdentityType.Inventory))
            {
                if (item2.Name.Contains("Cell Templates")
                    || item2.Name.Contains("Plasmid Cultures")
                    || item2.Name.Contains("Mitochondria Samples")
                    || item2.Name.Contains("Protein Mapping Data")
                    || item2.Name.Contains("Mission Token")) item2?.Use();
            }

            if (Inventory.NumFreeSlots >= 1)
            {
                item.MoveToInventory();
                return;
            }

            List<Item> bags = Inventory.Items
            .Where(c => c.UniqueIdentity.Type == IdentityType.Container)
            .ToList();

            foreach (Item bag in bags)
            {
                bag.Use();
                bag.Use();
            }

            foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot == IdentityType.Inventory).Where(c => !IgnoreItems.Contains(c.Name)))
            {
                if (itemtomove.Name != "Health and Nano Recharger" && itemtomove.Name != "Health and Nano Stim"
                    && itemtomove.Name != "Aggression Enhancer" && itemtomove.Name != "Tree of Enlightenment" && itemtomove.Name != "Ape Fist of Khalum" && itemtomove.Name != "Lock Pick"
                    && itemtomove.Name != "Codex of the Insulting Emerto"
                    && !itemtomove.Name.Contains("Ammo")
                    && !itemtomove.Name.Contains("Experience")
                    && !itemtomove.Name.Contains("Nano Can")
                    && !itemtomove.Name.Contains("Trimmer")
                    && !itemtomove.Name.Contains("Premium")
                    && !itemtomove.Name.Contains("Crate")
                    && !itemtomove.Name.Contains("Mission")
                    )
                {
                    //foreach (Backpack backpack in Inventory.Backpacks.Where(c => c.Name.Contains("a")))
                    foreach (Backpack backpack in Inventory.Backpacks)
                    {
                        Chat.WriteLine("Backpack:" + backpack.Name);
                        if (backpack.Items.Count < 21)
                        {

                            Chat.WriteLine("Move Item to Backpack:" + backpack.Name);
                            Chat.WriteLine("Backpack item count:" + backpack.Items.Count);
                            itemtomove.MoveToContainer(backpack);
                            break;
                        }

                    }

                }
            }
            item.MoveToInventory();
            if (item.Name.Contains("Cell Templates") || item.Name.Contains("Plasmid Cultures"))
            {
                item.Use(DynelManager.LocalPlayer);
            }
        }


        private void AddIgnoreItem(object s, ButtonBase button)
        {
            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("IgnoreItem", out TextInputView textinput1);

                if (textinput1 != null && textinput1.Text != String.Empty)
                {
                    Task.Factory.StartNew(
                     async () =>
                     {
                         await Task.Delay(200);

                         IgnoreItems.Add(textinput1.Text);
                         Chat.WriteLine($"Ignore item {textinput1.Text} added.");
                     });
                }
            }
        }

        private void ClearIgnoreList(object s, ButtonBase button)
        {
            Chat.WriteLine($"List cleared.");
            IgnoreItems.Clear();
        }

        private void PrintIgnoreList(object s, ButtonBase button)
        {
            if (IgnoreItems.Count() == 0)
                Chat.WriteLine($"List empty.");

            foreach (string ignoreItem in IgnoreItems)
            {
                Chat.WriteLine($"{ignoreItem}");
            }
        }

        private void AddRules(object s, ButtonBase button)
        {
            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("QlMinBox", out TextInputView textinput1);
                SettingsController.settingsWindow.FindView("QlMaxBox", out TextInputView textinput2);
                SettingsController.settingsWindow.FindView("ItemNameBox", out TextInputView textinput3);

                if (textinput1 != null && textinput1.Text != String.Empty && int.TryParse(textinput1.Text, out int minQl)
                    && textinput2 != null && textinput2.Text != String.Empty && int.TryParse(textinput2.Text, out int maxQl)
                    && textinput3 != null && textinput3.Text != String.Empty)
                {
                    Task.Factory.StartNew(
                     async () =>
                     {
                         await Task.Delay(200);

                         LootingRules.Add(minQl, maxQl, textinput3.Text);
                         Chat.WriteLine($"Rule added.");
                     });
                }
            }
        }

        private void ClearRules(object s, ButtonBase button)
        {
            Chat.WriteLine($"List cleared.");
            LootingRules.Clear();
        }

        private void PrintRules(object s, ButtonBase button)
        {
            LootingRules.DumpToChat();
        }

        private void HelpBox(object s, ButtonBase button)
        {
            Window helpWindow = Window.CreateFromXml("Help", PluginDirectory + "\\UI\\LootBuddyHelpBox.xml",
            windowSize: new Rect(0, 0, 455, 345),
            windowStyle: WindowStyle.Default,
            windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);
            helpWindow.Show(true);
        }

        private void OnUpdate(object sender, float deltaTime)
        {
            try
            {
                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    //SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView textinput1);

                    //if (textinput1 != null && textinput1.Text != String.Empty)
                    //{
                    //    if (int.TryParse(textinput1.Text, out int channelValue))
                    //    {
                    //        if (Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                    //        {
                    //            IPCChannel.SetChannelId(Convert.ToByte(channelValue));
                    //            Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToByte(channelValue);
                    //            SettingsController.LootBuddyChannel = channelValue.ToString();
                    //            Config.Save();
                    //        }
                    //    }
                    //}

                    if (SettingsController.settingsView != null)
                    {
                        if (SettingsController.settingsView.FindChild("LootBuddyHelpBox", out Button helpBox))
                        {
                            helpBox.Tag = SettingsController.settingsView;
                            helpBox.Clicked = HelpBox;
                        }

                        if (SettingsController.settingsView.FindChild("AddList", out Button addBox))
                        {
                            addBox.Tag = SettingsController.settingsView;
                            addBox.Clicked = AddRules;
                        }

                        if (SettingsController.settingsView.FindChild("PrintList", out Button printBox))
                        {
                            printBox.Tag = SettingsController.settingsView;
                            printBox.Clicked = PrintRules;
                        }

                        if (SettingsController.settingsView.FindChild("ClearList", out Button clearBox))
                        {
                            clearBox.Tag = SettingsController.settingsView;
                            clearBox.Clicked = ClearRules;
                        }

                        if (SettingsController.settingsView.FindChild("AddIgnoreItem", out Button addIgnoreBox))
                        {
                            addIgnoreBox.Tag = SettingsController.settingsView;
                            addIgnoreBox.Clicked = AddIgnoreItem;
                        }

                        if (SettingsController.settingsView.FindChild("PrintIgnoreList", out Button printIgnoreBox))
                        {
                            printIgnoreBox.Tag = SettingsController.settingsView;
                            printIgnoreBox.Clicked = PrintIgnoreList;
                        }

                        if (SettingsController.settingsView.FindChild("ClearIgnoreList", out Button clearIgnoreBox))
                        {
                            clearIgnoreBox.Tag = SettingsController.settingsView;
                            clearIgnoreBox.Clicked = ClearIgnoreList;
                        }
                    }
                }


                //if (SettingsController.LootBuddyChannel == String.Empty)
                //{
                //    SettingsController.LootBuddyChannel = Config.IPCChannel.ToString();
                //}

                if (LootBuddySettings["Running"].AsBool())
                {
                    if (Time.NormalTime - _lastCheckTime > new Random().Next(1, 6))
                    {
                        _lastCheckTime = Time.NormalTime;

                        corpsesToLoot = DynelManager.Corpses
                            .Where(corpse => corpse.DistanceFrom(DynelManager.LocalPlayer) < 7)
                            .FirstOrDefault();

                        if (corpsesToLoot != null)
                        {
                            corpsesToLoot.Open();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine(e);
            }
        }

        //private void LootBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        //{
        //    try
        //    {
        //        if (param.Length < 1)
        //        {
        //            if (!LootBuddySettings["Running"].AsBool())
        //            {
        //                LootBuddySettings["Running"] = true;
        //                Chat.WriteLine("Bot enabled.");
        //            }
        //            else
        //            {
        //                Chat.WriteLine("Bot disabled.");
        //                LootBuddySettings["Running"] = false;
        //            }
        //            return;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Chat.WriteLine(e.Message);
        //    }
        //}

        public override void Teardown()
        {
            LootingRules.Save();
        }
    }
}
